import React, {Fragment} from 'react';
import blog_02_picture from '../../assets/close-table-technology-notebook-open.jpg';

const Blog_02 = () => {
	return (
		<Fragment>
				<div className="blog_wrapper_wrapper_2">
					<img src={blog_02_picture} alt="close-table-technology-notebook-open.jpg"/>
				</div>
				<h2 className="blog_subtitle_2">//Title 01</h2><span id="author_2">author. 01.09.2020</span>
				<h3 className="sec_title_2">Secondary Title</h3>
				<p className="blog_text_inside_2"><br/>Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea<br/>commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum<br/>dolore eu fugiat nulla pariatur.</p>
				<div>
					<a className="link_read_more_2" href = "">Read more <span>&gt;</span></a>
				</div>
		</Fragment>
	);
}

export default Blog_02;