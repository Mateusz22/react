import React, {Fragment} from 'react';
import circle_04 from '../../assets/koła_03.png';

const Skills = () => {
	return (
		<Fragment>
			<div className="circle_04">
				<img src={circle_04} alt="koła_03.png"/>
			</div>
				<h2 className="skills" id="C_2">//Skills</h2>
				<p className="sub_4">Duis aute irure dolor in reprehenderit in voluptate velit<br/>esse cillum dolore eu fugiat nulla pariatur.</p>
				<div>
						<ol>
							<li className="graph_1"><span className="graph_text">PHP 100%</span></li>
							<li className="graph_2"><span className="graph_text">JS 90%</span></li>
							<li className="graph_3"><span className="graph_text">HTML 90%</span></li>
							<li className="graph_4"><span className="graph_text">NODEJS 60%</span></li>
							<li className="graph_5"><span className="graph_text">CSS 90%</span></li>
							<li className="graph_6"><span className="graph_text">GO 60%</span></li>
						</ol>
				</div>
		</Fragment>
	);
}

export default Skills;