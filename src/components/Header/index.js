import React, {Fragment} from 'react';
import initials from '../../assets/JD2.png';
import twitter from '../../assets/twitter_icon.png';
import facebook from '../../assets/facebook_icon.png';
import linkedin from '../../assets/linkedin_icon.png';

const Header = () => {
	return (
		<header>
				<div className="main-nav__wrapper">
					<div>
						<img src={initials} alt="Initials" id="header_initials"/>
					</div>
					<nav>
					<ol className="background-list">
						<li className="list-link"><a className="link-space" href="#C_1">About me</a></li>
						<li className="list-link"><a className="link-space" href="#C_2">Skills</a></li>
						<li className="list-link"><a className="link-space" href="#C_3">Portfolio</a></li>
						<li className="list-link"><a className="link-space" href="#C_4">Blog</a></li>
						<li className="list-link"><a className="link-space" href="#C_5">Contact me</a></li>
						<span className="stream">|</span>
						<li className="list-image"><a href="https://twitter.com/?lang=pl" target="__blank"><img src={twitter}/></a></li>
						<li className="list-image"><a href="https://pl-pl.facebook.com/" target="__blank"><img src={facebook}/></a></li>
						<li className="list-image"><a href="https://pl.linkedin.com/" target="__blank"><img src={linkedin}/></a></li>
					</ol>
					</nav>
				</div>
		</header>
	);
}

export default Header;