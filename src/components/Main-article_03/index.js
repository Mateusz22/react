import React, {Fragment} from 'react';
import code_button from '../../assets/easy_code_button.png';

const Main_article_03 = () => {
	return(
			<Fragment>
				<h2>//About me</h2>
				<h3 className="sub_0">All about Techy</h3>
				<p className="sub_1">Lorem ipsum dolor sit amet,<br/><br/> consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat.<br/><br/> Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur.</p>
				<h3 className="sub_2">My interests</h3>
				<ul className="sub_3">
					<li>music</li>
					<li>kitesurfing</li>
					<li>cycling</li>
				</ul>
				<p className="code">Ukończyłem kurs Easy Code</p>
				<div className="code_button">
					<img src={code_button} alt="easy_code_button.png"/>
				</div>
			</Fragment>
	);
}
export default Main_article_03;