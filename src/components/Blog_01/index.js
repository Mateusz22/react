import React, {Fragment} from 'react';
import circle_08 from '../../assets/koła_01.png';
import blog_01_picture from '../../assets/caucasian-man-working-shoot1.jpg';

const Blog_01 = () => {
	return (
			<Fragment>
				<h2 className ="blog_title" id="C_4">//Blog posts</h2>
				<h3 className="blog_text">Hints and tips</h3>
				<div className="circle_08">
					<img src={circle_08} alt="koła_01.png"/>
				</div>
				<div className="blog_content">
					<div className="blog_wrapper_wrapper">
						<img src={blog_01_picture} alt="caucasian-man-working-shoot1.jpg"/>
					</div>
					<h2 className="blog_subtitle_1">//Title 01</h2><span id="author_1">author. 01.09.2020</span>
					<h3 className="sec_title_1">Secondary Title</h3>
					<p className="blog_text_inside_1"><br/>Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea<br/>commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum<br/>dolore eu fugiat nulla pariatur.</p>
					<div>
						<a className="link_read_more_1" href = "">Read more <span>&gt;</span></a>
					</div>
				</div>
			</Fragment>
	);
} 

export default Blog_01;