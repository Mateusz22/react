import React, {Fragment} from 'react';
import circle_05 from '../../assets/koła_01.png';
import circle_06 from '../../assets/koła_03.png';
import tools_icon_1 from '../../assets/_react_icon_s.png';
import tools_icon_2 from '../../assets/webpack_icon.png';
import tools_icon_3 from '../../assets/express_icon.png';
import tools_icon_4 from '../../assets/styled_components_icon.png';
import tools_icon_5 from '../../assets/redux_icon_s.png';
import tools_icon_6 from '../../assets/flexbox_icon_s.png';
import tools_icon_7 from '../../assets/tool_program_ikonaa.png';
import tools_icon_8 from '../../assets/tool_program_ikonaa.png';

const Tools = () => {
	return (
			<Fragment>
				<div className="circle_06">
					<img src={circle_06} alt="koła_03.png"/>
				</div>
				<h2 className="tools">//Tools</h2>
				<h3 className="tools_1">My essentials</h3>
				<div className="tools_2">
					<div className="tools_icon_1">
						<p><img src={tools_icon_1} alt="_react_icon_s.png"/><br/>React<br/>16.6.3</p>
					</div>
					<div className="tools_icon_2">
						<p><img img src={tools_icon_2} alt="webpack_icon.png"/><br/>Webpack<br/>4.19.1</p>
					</div>
					<div className="tools_icon_3">
						<p><img src={tools_icon_3} alt="express_icon.png"/><br/>Express<br/>4.16.4</p>
					</div>
					<div className="tools_icon_4">
						<p><img src={tools_icon_4} alt="styled_components_icon.png"/><br/>Styled<br/>Components<br/>4.16.4</p>
					</div>
					<div className="tools_icon_5">
						<p><img src={tools_icon_5} alt="redux_icon_s.png"/><br/>Redux<br/>4.0.1</p>
					</div>
					<div className="tools_icon_6">
						<p><img src={tools_icon_6} alt="flexbox_icon_s.png"/><br/>Flexbox<br/>4.0.1</p>
					</div>
					<div className="tools_icon_7">
						<p><img src={tools_icon_7} alt="tool_program_ikona.png"/><br/>Program<br/>5.2.1</p>
					</div>
					<div className="tools_icon_8">
						<p><img src={tools_icon_8} alt="tool_program_ikona.png"/><br/>Program<br/>5.2.2</p>
					</div>
				</div>
				<div className="circle_05">
					<img src={circle_05} alt="koła_01.png"/>
				</div>
			</Fragment>
	);
}

export default Tools;