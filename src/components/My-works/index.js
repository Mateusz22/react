import React, {Fragment} from 'react';
import circle_07 from '../../assets/koła_01.png';
import github_icon from '../../assets/github_icon.png';
import external_link_icon from '../../assets/external_link_icon.png';

const My_works = () => {
	return (
		<Fragment>
				<h2 className="my_works_1" id="C_3">//My works</h2>
				<h3 className="my_works_2">Portfolio</h3>
				<p className="my_works_3"><br/>Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris<br/> nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in<br/> reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla<br/>pariatur.</p>
				<div className="circle_07">
					<img src={circle_07} alt="koła_01.png"/>
				</div>
				<div className="portfolio_1">
					<img src="" alt=""/>
					<div className="icons_1">
						<img src={github_icon} alt="github_icon.png"/><img src={external_link_icon} alt="external_link_icon.png"/>
					</div>
				</div>
				<div className="portfolio_2">
					<img src="" alt=""/>
					<div className="icons_1">
						<img src={github_icon} alt="github_icon.png"/><img src={external_link_icon} alt="external_link_icon.png"/>
					</div>
				</div>
				<div className="portfolio_3">
					<img src="" alt=""/>
					<div className="icons_1">
						<img src={github_icon} alt="github_icon.png"/><img src={external_link_icon} alt="external_link_icon.png"/>
					</div>
				</div>
				<div className="portfolio_4">
					<img src="" alt=""/>
					<div className="icons_1">
						<img src={github_icon} alt="github_icon.png"/><img src={external_link_icon} alt="external_link_icon.png"/>
					</div>
				</div>
				<div className="portfolio_5">
					<img src="" alt=""/>
					<div className="icons_1">
						<img src={github_icon} alt="github_icon.png"/><img src={external_link_icon} alt="external_link_icon.png"/>
					</div>
				</div>
				<div className="portfolio_6">
					<img src="" alt=""/>
					<div className="icons_1">
						<img src={github_icon} alt="github_icon.png"/><img src={external_link_icon} alt="external_link_icon.png"/>
					</div>
				</div>
		</Fragment>
	);
}

export default My_works;