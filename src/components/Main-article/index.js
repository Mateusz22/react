import React, {Fragment} from 'react';
import myPicture from '../../assets/myPicture.jpg';
import circle_01 from '../../assets/koła_03.png';
import bitbucket from '../../assets/bit_bucket_icon.png';
import dev_icon from '../../assets/dev_icon.png';

const Main_article = () => {
return (
		<Fragment>
			<div className="picture_01">
				<img src={myPicture} alt="myPicture.jpg"/>
			</div>
			<div className="circle_01">
				<img src={circle_01} alt="koła_03.png"/>
			</div>
			<div className="main-header_article_01" id="C_1">
					<h1>//Hi, My name is John Doe</h1>
					<span className="subtitle"><br/>Software Engineer</span>
					<p className="subtitle_1">Passionate Techy and Tech Author<br/>
					with 3 years of experience within the field.</p>
					<p className="subtitle_2">See my works</p>
					<div className="bitbucket_icon">
						<img src={bitbucket} alt="bit_bucket_icon.png"/>
					</div>
					<div className="dev_icon">
						<img src={dev_icon} alt="dev_icon.png"/>
					</div>
				</div>
		</Fragment>
	);
}
export default Main_article;