import circle_03 from '../../assets/koła_01.png';
import circle_02 from '../../assets/koła_02.png';

const Main_article_02 = () => {
	return (
		<div className="main-header_article_02">
				<h2 className="subtitle_3">// I am a freelancer</h2>
				<p className="subtitle_4">Contact me if you want to work with me</p>
				<a className="mail" href = "mailto: gmail.com">Hire me</a>
				<a className="download" href = "">Download CV</a>
				<div className="circle_03">
					<img src={circle_03} alt="koła_01.png"/>
				</div>
				<div className="circle_02">
					<img src={circle_02} alt="koła_02.png"/>
				</div>
		</div>
	);
}
export default Main_article_02;