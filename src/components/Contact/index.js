import React, {Fragment} from 'react';
import circle_09 from '../../assets/koła_03.png';
import circle_10 from '../../assets/koła_03.png';
import my_contact_icon from '../../assets/kontakt_ikona2.png';
import my_button from '../../assets/kontakt_ikona_3.png';
import myPicture_02 from '../../assets/myPicture.jpg';

const Contact = ()=> {
	return (
		<Fragment>
				<div className="circle_10">
					<img src={circle_10} alt="koła_03.png"/>
				</div>
				<div className="my_contact">
					<div className="my_contact_icon">
						<img src={my_contact_icon} alt="kontakt_ikona2.png"/>
					</div>
					<p className="my_contact_text">john_doe@gmail.com<br/>+ 32 123 345 567</p>
				</div>
				<h2 className="contact_text_1" id="C_5">//Contact me</h2>
				<p className="contact_text_2"><br/>If you are willing to work with me, contact me. I can join your<br/>conference to serve you with my engineering experience.</p>
				<form>
					<div className="input_1">
						<label></label>
						<input type="text" name="item_1" placeholder="Your e-mail"/>
						<br /><br />
					</div>
					<div className="input_2">
						<label></label>
						<input type="text" name="item_2" placeholder="Your name"/>
						<br /><br />
					</div>
					<div className="input_3">
						<label></label>
						<textarea id="input_id_3" cols="30" rows="11" name="item_3" placeholder="How can I help you?&#10;Please, put here your&#10;message/request."></textarea>
					</div>
					<input className="btn" id="my_button" type="image" src={my_button}/>
				</form>
				<div className="my_picture_2_text">
					<p>autor: John Doe<br/>
						username: @johndoe<br/>
						description: University Graduate | Software Engineer<br/>
						homepage: johndoe.github.pl<br/>
						repository type: Open-source<br/>
					url: github.com/johndoe</p>
				</div>
				<div className="my_picture_2">
					<img src={myPicture_02} alt="myPicture.jpg"/>
				</div>
				<div className="circle_09">
					<img src={circle_09} alt="koła_03.png"/>
				</div>
		</Fragment>
	);
}

export default Contact;