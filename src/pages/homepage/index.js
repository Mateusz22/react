import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';
import '../../App.css';


const Header = React.lazy(() => import('../../components/Header'));
const Main_article = React.lazy(() => import('../../components/Main-article'));
const Main_article_02 = React.lazy(() => import('../../components/Main-article_02'));
const Main_article_03 = React.lazy(() => import('../../components/Main-article_03'));
const Skills = React.lazy(() => import('../../components/Skills'));
const Tools = React.lazy(() => import('../../components/Tools'));
const My_works = React.lazy(() => import('../../components/My-works'));
const Blog_01 = React.lazy(() => import('../../components/Blog_01'));
const Blog_02 = React.lazy(() => import('../../components/Blog_02'));
const Contact = React.lazy(() => import('../../components/Contact'));



const Homepage = () => {
	return (
	  	<Suspense fallback= {<div><h2>Loading...</h2></div>}>
		<div className="main--wrapper">
			<Header />
			<div className="main-content__wrapper">
				<Main_article/>
				<Main_article_02/>
			</div>
			<div className="main-article_wrapper">
				<Main_article_03/>
			</div>
			<div className="div_skills">
				<Skills/>
			</div>
			<div className="tools_bar">
				<Tools/>
			</div>
			<div className="my_works_main">
				<My_works/>
			</div>
			<div className="blog_wrapper">
				<Blog_01/>
					<div className="blog_content_2">
						<Blog_02/>
					</div>
			</div>
			<div className="contact_wrapper">
				<Contact/>
			</div>
		</div>
	</Suspense>
	);
};

export default Homepage;